import React, { useState, useEffect } from 'react';

import './profileViewStyle.scss';
import Post from 'components/Post';
import Navbar from 'components/Navbar';
import { ReactComponent as EmailIcon } from 'assets/img/email.svg';
import { ReactComponent as LikeIcon } from 'assets/img/like.svg';
import { ReactComponent as CheckIcon } from 'assets/img/check.svg';
import { ReactComponent as PastIcon } from 'assets/img/past.svg';

import { ReactComponent as LikeOutlineIcon } from 'assets/img/like-outline.svg';
import { ReactComponent as CheckOutlineIcon } from 'assets/img/check-outline.svg';
import { ReactComponent as PastOutlineIcon } from 'assets/img/past-outline.svg';

import faker from 'faker';

import Placeholder from 'assets/img/user.svg';
import Spinner from 'components/Spinner';
import EmptyPost from 'components/EmptyPost';
import InfiniteScroll from 'react-infinite-scroller';

import { fetchPostList } from 'containers/ListView/actions';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { withRouter } from 'react-router';

type ProfileViewProps = {
  fetchPostList: () => any;
  fetchPostListResponse: Record<string, unknown>[];
  isLoading: boolean;
  isFetchPostListSuccess: boolean;
  history: any;
};

const ProfileView: React.FC<ProfileViewProps> = (props) => {
  const LIKES = 'LIKES';
  const GOING = 'GOING';
  const PAST = 'PAST';

  const likeCount = 12;
  const goingCount = 0;
  const pastCount = 0;
  const [currTab, setCurrTab] = useState(LIKES);

  const [postList, setPostList] = useState(props.fetchPostListResponse);

  useEffect(() => {
    props.fetchPostList();
  }, []);

  useEffect(() => {
    const tempList: Record<string, unknown>[] = [];
    if (postList.length > 1) {
      postList.forEach((post) => {
        tempList.push(post);
      });
    }
    props.fetchPostListResponse.forEach((post) => {
      tempList.push(post);
    });
    setPostList(tempList);
  }, [props.fetchPostListResponse]);

  let renderPost: any = [];
  if (postList.length > 1) {
    postList.forEach((post: any, index: number) => {
      renderPost.push(
        <div className="item-wrapper" key={`${index};${post.title};${post.username}`}>
          <Post
            id={post.id}
            username={post.user.username}
            channelName={post.user.channel_name}
            avatar={post.user.avatar}
            title={post.post_title}
            dateEnd={post.date_end}
            dateStart={post.date_start}
            postPic={post.post_pic}
            description={post.description}
            likeCount={post.like_count}
            goingCount={post.going_count}
            onTitleClick={() => props.history.push(`/details/${post.id}`)}
            onAvatarClick={() => props.history.push(`/profile/${post.id}`)}
          />
        </div>,
      );
    });
  }
  switch (currTab) {
    case LIKES:
      <InfiniteScroll initialLoad={false} loadMore={props.fetchPostList} hasMore={true} loader={<Spinner key={0} />}>
        {likeCount > 0
          ? renderPost
          : (renderPost = (
              <div className="empty-wrap">
                <EmptyPost />
              </div>
            ))}
      </InfiniteScroll>;
      break;
    case GOING:
      <InfiniteScroll initialLoad={false} loadMore={props.fetchPostList} hasMore={true} loader={<Spinner key={0} />}>
        {goingCount > 0
          ? renderPost
          : (renderPost = (
              <div className="empty-wrap">
                <EmptyPost />
              </div>
            ))}
      </InfiniteScroll>;
      break;
    case PAST:
      <InfiniteScroll initialLoad={false} loadMore={props.fetchPostList} hasMore={true} loader={<Spinner key={0} />}>
        {pastCount > 0
          ? renderPost
          : (renderPost = (
              <div className="empty-wrap">
                <EmptyPost />
              </div>
            ))}
      </InfiniteScroll>;
      break;
    default:
      break;
  }

  return (
    <>
      <Navbar isRoot={false} />
      <div className="profile-view-wrap">
        <div className="profile-section">
          <div className="profile-avatar-container">
            <img className="profile-picture" src={faker.internet.avatar()} alt="avatar-placeholder" />
          </div>
          <p className="profile-username">Username</p>
          <div className="email-wrap">
            <div>
              <EmailIcon />
            </div>
            <div className="profile-email">myusername@gmail.com</div>
          </div>
        </div>
        <div className="likes-going-past-box">
          <div className={`likes-wrap ${currTab === LIKES ? 'lgp-active' : ''}`} onClick={() => setCurrTab(LIKES)}>
            {currTab === LIKES ? <LikeIcon /> : <LikeOutlineIcon />}

            <div className="likes-text">{likeCount} Likes</div>
          </div>
          <div className={`going-wrap ${currTab === GOING ? 'lgp-active' : ''}`} onClick={() => setCurrTab(GOING)}>
            {currTab === GOING ? <CheckIcon /> : <CheckOutlineIcon />}

            <div className="going-text">{goingCount} Going</div>
          </div>
          <div className={`past-wrap ${currTab === PAST ? 'lgp-active' : ''}`} onClick={() => setCurrTab(PAST)}>
            {currTab === PAST ? <PastIcon /> : <PastOutlineIcon />}

            <div className="past-text">{pastCount} Past</div>
          </div>
        </div>{' '}
        <div className="personal-post-list-wrap"></div>
      </div>
      <div className="list-view-wrap">{renderPost}</div>
    </>
  );
};

function mapStateToProps(state: any) {
  return {
    fetchPostListResponse: state.listViewReducer.get('fetchPostListResponse'),
    isLoading: state.listViewReducer.get('isLoading'),
    isFetchPostListSuccess: state.listViewReducer.get('isFetchPostListSuccess'),
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    fetchPostList: () => dispatch(fetchPostList()),
    push: (url: string) => dispatch(push(url)),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProfileView));
