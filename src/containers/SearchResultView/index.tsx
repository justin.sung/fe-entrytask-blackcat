import React, { useEffect, useState } from 'react';

import Navbar from 'components/Navbar';

import './searchResultViewStyle.scss';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import Spinner from 'components/Spinner';
import InfiniteScroll from 'react-infinite-scroller';
import { fetchPostList } from 'containers/ListView/actions';
import Post from 'components/Post';
import EmptyPost from 'components/EmptyPost';

type SearchResultViewProps = {
  fetchPostList: () => any;
  fetchPostListResponse: Record<string, unknown>[];
  isLoading: boolean;
  isFetchPostListSuccess: boolean;
  searchQuery: string;
  match: any;
  history: any;
};

const SearchResultView: React.FC<SearchResultViewProps> = (props) => {
  const [postList, setPostList] = useState(props.fetchPostListResponse);
  // const [searchResponse, setSearchResponse] = useState([]);
  const [resultCount, setResultCount] = useState(0);
  const [channel, setChannel] = useState('');
  const [dateFrom, setDateFrom] = useState('');
  const [dateTo, setDateTo] = useState('');
  const [whichTime, setWhichTime] = useState('');

  useEffect(() => {
    // get data from params to query post
    const params = props.match.params;
    let tempChannel = params.channel;
    if (tempChannel === 'All') {
      tempChannel += ' Channel';
    }
    setChannel(tempChannel);
    if (params.from) {
      setDateFrom(params.from.replace('-', '/'));
      setDateTo(params.from.replace('-', '/'));
    }
    setWhichTime(params.whichTime);
    setResultCount(0);
    // search post using actions
    props.fetchPostList();
  }, []);

  useEffect(() => {
    const tempList: Record<string, unknown>[] = [];
    console.log(postList);

    if (postList.length > 1) {
      postList.forEach((post) => {
        tempList.push(post);
      });
    }
    props.fetchPostListResponse.forEach((post) => {
      tempList.push(post);
    });
    console.log(tempList[0]);
    setPostList(tempList);
    setResultCount(tempList.length); //somehow the list when init is already 1?
  }, [props.fetchPostListResponse]);

  const renderPost: any = [];
  if (postList.length > 1) {
    postList.forEach((post: any, index: number) => {
      renderPost.push(
        <div className="item-wrapper" key={`${index};${post.title};${post.username}`}>
          <Post
            id={post.id}
            username={post.user.username}
            channelName={post.user.channel_name}
            avatar={post.user.avatar}
            title={post.post_title}
            dateEnd={post.date_end}
            dateStart={post.date_start}
            postPic={post.post_pic}
            description={post.description}
            likeCount={post.like_count}
            goingCount={post.going_count}
            onTitleClick={() => props.history.push(`/details/${post.id}`)}
            onAvatarClick={() => props.history.push(`/profile/${post.id}`)}
          />
        </div>,
      );
    });
  }

  let searchSummary = '';
  if (whichTime === 'LATER') {
    searchSummary = `Searched for ${channel} activities from ${dateFrom} to ${dateTo}`;
  } else {
    searchSummary = `Searched for ${channel} activities for ${whichTime}`;
  }

  return (
    <>
      <Navbar isRoot={true} />
      <div className="search-result-view-wrap">
        <div className="search-summary-container">
          <div className="first-row">
            <div className="search-count">{resultCount} Results</div>
            <div className="clear-search-btn-container">
              <div className="clear-search" onClick={() => props.history.push('/')}>
                CLEAR SEARCH
              </div>
            </div>
          </div>
          <div className="second-row">
            <p className="search-summary">{searchSummary}</p>
          </div>
        </div>
        <div className="list-view-wrap">
          {resultCount > 0 ? (
            <InfiniteScroll
              initialLoad={false}
              loadMore={props.fetchPostList}
              hasMore={true}
              loader={<Spinner key={0} />}
            >
              {renderPost}
            </InfiniteScroll>
          ) : (
            <div className="empty-wrap">
              <EmptyPost />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

function mapStateToProps(state: any) {
  return {
    fetchPostListResponse: state.listViewReducer.get('fetchPostListResponse'),
    isLoading: state.listViewReducer.get('isLoading'),
    isFetchPostListSuccess: state.listViewReducer.get('isFetchPostListSuccess'),
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    fetchPostList: () => dispatch(fetchPostList()),
    push: (url: string) => dispatch(push(url)),
    // clearLogin: () => dispatch(clearLogin()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchResultView);
