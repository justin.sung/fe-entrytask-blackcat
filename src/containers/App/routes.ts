import React from 'react';
const NotFoundPage = React.lazy(() => import('containers/NotFoundPage'));
const LoginView = React.lazy(() => import('containers/LoginView'));
const ListView = React.lazy(() => import('containers/ListView'));
const DetailView = React.lazy(() => import('containers/DetailView'));
const ProfileView = React.lazy(() => import('containers/ProfileView'));
const SearchResultView = React.lazy(() => import('containers/SearchResultView'));
const BabylonPage = React.lazy(() => import('containers/BabylonPage'));

const routes: Record<string, any | boolean | string>[] = [
  {
    comp: LoginView,
    path: '/login',
  },
  {
    comp: BabylonPage,
    path: '/test/babylon',
    protected: true,
  },
  {
    comp: SearchResultView,
    path: '/search/:channel/:whichTime?/:from?/:to?',
    protected: true,
  },
  {
    comp: ProfileView,
    path: '/profile/:id',
    protected: true,
  },
  {
    comp: DetailView,
    path: '/details/:id',
    protected: true,
  },
  {
    comp: ListView,
    path: '/',
    protected: true,
  },
  {
    comp: NotFoundPage,
  },
];

export default routes;
