import React, { useState } from 'react';

import Navbar from 'components/Navbar';
import ChannelBox from 'components/ChannelBox';
import faker from 'faker';
import './detailViewStyle.scss';
import styles from 'global.module.scss';

import { ReactComponent as ParticipantIcon } from 'assets/img/people-outline.svg';
import { ReactComponent as DetailIcon } from 'assets/img/info.svg';
import { ReactComponent as CommentIcon } from 'assets/img/comment.svg';

import CommentSection from 'components/CommentSection';
import BottomWidget from 'components/BottomWidget';
import ParticipantContent from 'components/ParticipantContent';

import DetailContent from 'components/DetailContent';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { withRouter } from 'react-router';

type DetailViewProps = { history: any };

const DetailView: React.FC<DetailViewProps> = (props) => {
  const DETAILS = 'DETAILS';
  const PARTICIPANTS = 'PARTICIPANTS';
  const COMMENTS = 'COMMENTS';

  const [currTab, setCurrTab] = useState(DETAILS);

  const tempPic = faker.internet.avatar();
  const pictureList = [faker.image.image(), faker.image.image(), faker.image.image()];
  const goingThumbAvatarList = [
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
  ];
  const likedThumbAvatarList = [
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
    faker.internet.avatar(),
  ];

  let renderContent = null;
  switch (currTab) {
    case DETAILS:
      renderContent = (
        <>
          <DetailContent
            pictureList={pictureList}
            goingThumbAvatarList={goingThumbAvatarList}
            likedThumbAvatarList={likedThumbAvatarList}
          />
          <BottomWidget type={DETAILS} />
        </>
      );
      break;
    case COMMENTS:
      renderContent = (
        <>
          <CommentSection focus={true} />
          <BottomWidget type={COMMENTS} />
        </>
      );
      break;

    case PARTICIPANTS:
      renderContent = (
        <>
          <ParticipantContent
            goingThumbAvatarList={goingThumbAvatarList}
            likedThumbAvatarList={likedThumbAvatarList}
            minimized={false}
            focus={false}
          />
          <BottomWidget type={COMMENTS} />
        </>
      );
      break;
    default:
      break;
  }

  return (
    <>
      <Navbar isRoot={false} />
      <div className="detail-view-wrap">
        <div className="title-section">
          <div className="channel-box-wrap">
            <ChannelBox channelName="Channel Name" />
          </div>
          <div className="title">
            <p className="title-text">Activity Title Name Make it Longer May Longer than One Line</p>
          </div>
          <div className="profile-section">
            <div className="profile-picture-container" onClick={() => props.history.push(`/profile/1`)}>
              <img src={tempPic} alt="temp-profile-pic" />
            </div>
            <div className="profile-detail-wrap" onClick={() => props.history.push(`/profile/1`)}>
              <p className={`${styles.username} profile-detail-username`}>Username</p>
              <p className="last-publish">Published 2 days ago</p>
            </div>
          </div>
        </div>
        <div className="details-participant-comment-box">
          <div
            className={`details-wrap ${currTab === DETAILS ? 'dpc-active' : ''}`}
            onClick={() => setCurrTab(DETAILS)}
          >
            <DetailIcon />
            <div className="details-text">Details</div>
          </div>
          <div
            className={`participant-wrap ${currTab === PARTICIPANTS ? 'dpc-active' : ''}`}
            onClick={() => setCurrTab(PARTICIPANTS)}
          >
            <ParticipantIcon />
            <div className="participant-text">Participants</div>
          </div>
          <div
            className={`comment-wrap ${currTab === COMMENTS ? 'dpc-active' : ''}`}
            onClick={() => setCurrTab(COMMENTS)}
          >
            <CommentIcon />
            <div className="comment-text">Comments</div>
          </div>
        </div>

        <div className="detail-content-wrap">{renderContent}</div>
      </div>
    </>
  );
};

function mapDispatchToProps(dispatch: any) {
  return {
    push: (url: string) => dispatch(push(url)),
  };
}

export default withRouter(connect(null, mapDispatchToProps)(DetailView));
