import React, { useState } from 'react';

import './bottomWidgetStyle.scss';
import { ReactComponent as CommentSingleIcon } from 'assets/img/comment-single.svg';
import { ReactComponent as LikeOutlineIcon } from 'assets/img/like-outline.svg';
import { ReactComponent as CheckOutlineIcon } from 'assets/img/check-outline.svg';
import { ReactComponent as SendIcon } from 'assets/img/send.svg';
import { ReactComponent as CrossIcon } from 'assets/img/cross.svg';

type BottomWidgetProps = {
  type: string;
};

const BottomWidget: React.FC<BottomWidgetProps> = (props) => {
  const [newComment, setNewComment] = useState('');
  const PARTICIPANTS = 'PARTICIPANTS';
  const DETAILS = 'DETAILS';
  const COMMENTS = 'COMMENTS';

  function handleChange(e: any) {
    const target = e.target;

    switch (target.name) {
      case 'message':
        setNewComment(target.value);
        break;

      default:
        break;
    }

    // fieldValidation(target);
  }

  return (
    <div className="bottom-widget-wrap">
      {props.type === DETAILS ? (
        <div className="details-bottom-widget-wrap">
          <div className="comment-widget-btn widget">
            <CommentSingleIcon />
          </div>
          <div className="like-widget-btn widget">
            <LikeOutlineIcon />
          </div>
          <div className="join-widget-btn widget">
            <CheckOutlineIcon />
            <div>Join</div>
          </div>
        </div>
      ) : props.type === COMMENTS ? (
        <div className="comment-bottom-widget-wrap">
          <div className="msg-input-container">
            <div className="cross-container">
              <CrossIcon />
            </div>
            <div className="msg-input">
              <input type="text" name="message" placeholder="Leave your comment here" onChange={handleChange} />
            </div>
          </div>
          <div className="send-container">
            <SendIcon />
          </div>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default BottomWidget;
