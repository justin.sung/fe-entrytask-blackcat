import React from 'react';

import './detailContentStyle.scss';
import { ReactComponent as DateFromIcon } from 'assets/img/date-from.svg';
import { ReactComponent as DateToIcon } from 'assets/img/date-to.svg';
import Gmap from 'assets/img/gmap.png';

import ParticipantContent from 'components/ParticipantContent';

type DetailContentProps = {
  pictureList: string[];
  goingThumbAvatarList: string[];
  likedThumbAvatarList: string[];
};

const DetailContent: React.FC<DetailContentProps> = (props) => {
  const renderPictures: any = [];
  props.pictureList.forEach((image, idx) => {
    renderPictures.push(
      <div className="sliding-picture" key={`${idx};${image}`}>
        <img src={image} alt={image} />
      </div>,
    );
  });

  const renderGoingAvatars: any = [];
  props.goingThumbAvatarList.forEach((avatar, idx) => {
    renderGoingAvatars.push(
      <div className="thumb-picture" key={`${idx};${avatar}`}>
        <img src={avatar} alt={avatar} />
      </div>,
    );
  });

  const renderLikedAvatars: any = [];
  props.likedThumbAvatarList.forEach((avatar, idx) => {
    renderLikedAvatars.push(
      <div className="thumb-picture" key={`${idx};${avatar}`}>
        <img src={avatar} alt={avatar} />
      </div>,
    );
  });
  return (
    <div className="detail-content-container">
      <div className="detail-content">
        <div className="picture-slider">{renderPictures}</div>
        <div className="content-description">
          <div className="content-text">
            [No longer than 300 chars] Vivamus sagittis, diam in lobortis, sapien arcu mattis erat, vel aliquet sem urna
            et risus. Ut feugiat sapien mi potenti. Maecenas et enim odio. Nullam massa metus, varius quis vehicula sed,
            pharetra mollis erat. In quis viverra velit. Vivamus placerat, est nec hendrerit varius, enim dui hendrerit
            magna, ut pulvinar nibh lorem vel lacus. Mauris a orci iaculis, hendrerit eros sed, gravida leo. In dictum
            mauris vel augue varius there is south north asim
          </div>
          <div className="read-more-overlay">
            <div className="read-more-button">
              <p className="btn-label">VIEW ALL</p>
            </div>
          </div>
        </div>
      </div>
      <hr className="off-left" />
      <div className="when-section">
        <div className="when-label">
          <div className="box"></div>
          <div>When</div>
        </div>
        <div className="split">
          <div className="left">
            <div className="row">
              <DateFromIcon />
              <p className="date-start">15 April 2015</p>
            </div>
            <div className="time-row">
              <div className="hour">8:30</div>
              <div className="am-pm">am</div>
            </div>
          </div>
          <div className="separator"></div>
          <div className="right">
            <div className="row">
              <DateToIcon />
              <p className="date-end">15 April 2015</p>
            </div>
          </div>
        </div>
      </div>
      <hr className="off-left" />

      <div className="where-section">
        <div className="where-label">
          <div className="box"></div>
          <div>Where</div>
        </div>
        <div className="map-section">
          <p className="map-place-title">Marina Bay Sands</p>
          <p className="map-place-address">10 Bayfront Ave, S018956</p>
          <img src={Gmap} alt="google-map" className="map-img" />
        </div>
      </div>

      <ParticipantContent
        goingThumbAvatarList={props.goingThumbAvatarList}
        likedThumbAvatarList={props.likedThumbAvatarList}
        minimized={true}
        focus={false}
      />
    </div>
  );
};

export default DetailContent;
