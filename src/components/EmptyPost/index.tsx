import React from 'react';

import './emptyPostStyle.scss';
import { ReactComponent as NoActivity } from 'assets/img/no-activity.svg';

const EmptyPost: React.FC = () => {
  return (
    <div className="empty-post-wrap">
      <div className="empty-logo-container">
        <NoActivity />
      </div>
      <div className="empty-post-label">No activity found</div>
    </div>
  );
};

export default EmptyPost;
