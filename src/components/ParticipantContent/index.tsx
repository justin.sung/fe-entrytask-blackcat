import React from 'react';

import { ReactComponent as CheckOutlineIcon } from 'assets/img/check-outline.svg';
import { ReactComponent as LikeOutlineIcon } from 'assets/img/like-outline.svg';
import CommentSection from 'components/CommentSection';

import './participantContentStyle.scss';

type ParticipantContentProps = {
  goingThumbAvatarList: string[];
  likedThumbAvatarList: string[];
  minimized: boolean;
  focus: boolean;
};

const ParticipantContent: React.FC<ParticipantContentProps> = (props) => {
  const ROW_LIMIT = 8;
  const renderGoingAvatars: any = [];
  props.goingThumbAvatarList.forEach((avatar, idx) => {
    if (props.minimized && renderGoingAvatars.length === ROW_LIMIT) {
      return;
    }
    renderGoingAvatars.push(
      <div className="thumb-picture" key={`${idx};${avatar}`}>
        <img src={avatar} alt={avatar} />
      </div>,
    );
  });

  const renderLikedAvatars: any = [];
  props.likedThumbAvatarList.forEach((avatar, idx) => {
    if (props.minimized && renderLikedAvatars.length === ROW_LIMIT) {
      return;
    }
    renderLikedAvatars.push(
      <div className="thumb-picture" key={`${idx};${avatar}`}>
        <img src={avatar} alt={avatar} />
      </div>,
    );
  });
  return (
    <div className="participant-content-container">
      <div className={`${props.minimized ? 'minimized' : ''} like-going-section`}>
        <div className="going-section">
          <div className="going-number-wrap">
            <CheckOutlineIcon />
            <div>34 going</div>
          </div>
          <div className="avatars-containers">{renderGoingAvatars}</div>
        </div>
        <hr className="off-left" />
        <div className="like-section">
          <div className="like-number-wrap">
            <LikeOutlineIcon />
            <div>7 likes</div>
          </div>
          <div className="avatars-containers">{renderLikedAvatars}</div>
        </div>
      </div>

      <div className="comment-section">
        <CommentSection focus={props.focus} />
      </div>
    </div>
  );
};

export default ParticipantContent;
