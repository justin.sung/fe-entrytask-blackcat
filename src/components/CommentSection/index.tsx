import React from 'react';

import './commentSectionStyle.scss';
import faker from 'faker';
import Chat from 'components/Chat';

type CommentSectionProps = {
  focus: boolean;
};

const CommentSection: React.FC<CommentSectionProps> = (props) => {
  const commentLogJSON = [
    {
      username: faker.internet.userName(),
      avatar: faker.internet.avatar(),
      message: faker.lorem.sentences(),
      dateSend: '9 hours ago',
    },
    {
      username: faker.internet.userName(),
      avatar: faker.internet.avatar(),
      message: faker.lorem.sentences(),
      dateSend: '9 hours ago',
    },
    {
      username: faker.internet.userName(),
      avatar: faker.internet.avatar(),
      message: faker.lorem.sentences(),
      dateSend: '9 hours ago',
    },
    {
      username: faker.internet.userName(),
      avatar: faker.internet.avatar(),
      message: faker.lorem.sentences(),
      dateSend: '9 hours ago',
    },
    {
      username: faker.internet.userName(),
      avatar: faker.internet.avatar(),
      message: faker.lorem.sentences(),
      dateSend: '9 hours ago',
    },
  ];

  const renderComments: any = [];
  commentLogJSON.forEach((comment, idx) => {
    renderComments.push(
      <div className="comment-wrap" key={`${idx};${comment}`}>
        <Chat
          username={comment.username}
          avatar={comment.avatar}
          message={comment.message}
          dateSend={comment.dateSend}
          focus={props.focus}
        />
      </div>,
    );
  });
  return <div className={`comment-section-wrap ${props.focus ? 'scrollable' : ''}`}>{renderComments}</div>;
};

export default CommentSection;
