/* eslint-disable @typescript-eslint/no-empty-function */
import React, { MouseEvent, useState, useEffect } from 'react';
import './sideBarStyle.scss';
import { connect } from 'react-redux';
import { ReactComponent as SearchIcon } from 'assets/img/search.svg';
import { ReactComponent as DateFromIcon } from 'assets/img/date-from.svg';
import { ReactComponent as DateToIcon } from 'assets/img/date-to.svg';
import { push } from 'connected-react-router';
import { withRouter, RouteComponentProps } from 'react-router';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import 'moment/locale/en-au';

import MomentLocaleUtils from 'react-day-picker/moment';
import moment from 'moment';

type SideBarProps = {
  isOpened: boolean;
  closeMenu: () => void;
  history: any;
};

const SideBar: React.FC<SideBarProps & RouteComponentProps> = (props) => {
  const [selectedTime, setSelectedTime] = useState('');
  const [selectedChannel, setSelectedChannel] = useState('');
  const [dateFromDD, setDateFromDD] = useState('');
  const [dateFromMM, setDateFromMM] = useState('');
  const [dateFromYYYY, setDateFromYYYY] = useState('');

  const [dateToDD, setDateToDD] = useState('');
  const [dateToMM, setDateToMM] = useState('');
  const [dateToYYYY, setDateToYYYY] = useState('');

  const [channels, setChannels] = useState([
    'All',
    'Channel 3',
    'Channel 4',
    'Channel 1',
    'Channel 2',
    'Short',
    'Long',
    'Channel 7',
    'Channel 8',
    'Channel 5',
    'Channel Long Name',
    'Channel 6',
  ]);

  const [times, setTimes] = useState(['ANYTIME', 'TODAY', 'TOMMORROW', 'THIS WEEK', 'THIS MONTH']);

  function handleChannelClicked(event: MouseEvent) {
    const target = event.currentTarget;
    const channel = target.innerHTML;
    if (channel === selectedChannel) {
      setSelectedChannel('');
    } else {
      setSelectedChannel(channel);
    }
  }

  function handleTimeClicked(event: MouseEvent) {
    const target = event.currentTarget;
    const time = target.innerHTML;
    if (time === selectedTime) {
      setSelectedTime('');
    } else {
      setSelectedTime(time);
    }
  }

  function handleDateFromChange(day: any) {
    const formatted = formatDate(day);
    const splitted = formatted.split('/');
    setDateFromDD(splitted[0]);
    setDateFromMM(splitted[1]);
    setDateFromYYYY(splitted[2]);
  }

  function handleDateToChange(day: any) {
    const formatted = formatDate(day);
    const splitted = formatted.split('/');
    setDateToDD(splitted[0]);
    setDateToMM(splitted[1]);
    setDateToYYYY(splitted[2]);
  }

  function formatDate(date: any, format = 'L', locale = 'en-AU') {
    return moment(date)
      .locale(locale)
      .format(Array.isArray(format) ? format[0] : format);
  }

  function parseDate(str: string, format = 'L', locale = 'en-AU') {
    const m = moment(str, format, locale, true);
    if (m.isValid()) {
      return m.toDate();
    }
    return undefined;
  }

  function handleSearch() {
    console.log('st', dateFromDD);
    console.log('sc', selectedChannel);

    let redirectSearchUrl = '';
    if (selectedTime === 'LATER') {
      redirectSearchUrl = `/search/${selectedChannel}/${selectedTime}/${dateFromDD}-${dateFromMM}/${dateToDD}-${dateToMM}`;
      console.log(redirectSearchUrl);
      props.history.push(redirectSearchUrl);
    } else {
      redirectSearchUrl = `/search/${selectedChannel}/${selectedTime}`;
      console.log(redirectSearchUrl);
      props.history.push(redirectSearchUrl);
    }
  }

  useEffect(() => {
    // init date placeholder date to state
    const now = formatDate(new Date());
    const splitted = now.split('/');
    setDateToDD(splitted[0]);
    setDateToMM(splitted[1]);
    setDateFromDD(splitted[0]);
    setDateFromMM(splitted[1]);
  }, []);

  const renderChannels: any = [];

  channels.forEach((channel, index) => {
    const isChannelSelected = selectedChannel === channel;
    renderChannels.push(
      <div
        className={`${isChannelSelected ? 'channel-selected' : ''} channel`}
        key={`${index};${channel}`}
        onClick={handleChannelClicked}
      >
        {channel}
      </div>,
    );
  });

  const renderTimes: any = [];

  times.forEach((time, index) => {
    const isTimeSelected = selectedTime === time;
    renderTimes.push(
      <div
        className={`${isTimeSelected ? 'time-selected' : ''} time-label`}
        key={`${index};${time}`}
        onClick={handleTimeClicked}
      >
        {time}
      </div>,
    );
  });

  const allSelected = selectedTime !== '' && selectedChannel !== '';
  const isLaterSelected = selectedTime == 'LATER';

  return (
    <div className="side-bar-wrap">
      <div className={`${props.isOpened ? 'open' : ''} side-menu`}>
        <div className="side-bar-top">
          <div className="menu-label">
            <span>DATE</span>
          </div>
          <div className="time-tag-container">
            {renderTimes}
            <div className="later-wrap">
              <div className={`${isLaterSelected ? 'time-selected' : ''} later-label`} onClick={handleTimeClicked}>
                LATER
              </div>
              {isLaterSelected ? (
                <div className="later-input-wrap">
                  <DateFromIcon />
                  <DayPickerInput
                    formatDate={formatDate}
                    parseDate={parseDate}
                    placeholder={`${formatDate(new Date())}`}
                    dayPickerProps={{
                      locale: 'en-AU',
                      localeUtils: MomentLocaleUtils,
                    }}
                    onDayChange={handleDateFromChange}
                  />
                  <div className="stripe">-</div>
                  <DateToIcon />
                  <DayPickerInput
                    formatDate={formatDate}
                    parseDate={parseDate}
                    placeholder={`${formatDate(new Date())}`}
                    onDayChange={handleDateToChange}
                  />
                </div>
              ) : (
                ''
              )}
            </div>
          </div>

          <div className="menu-label">
            <span>CHANNEL</span>
          </div>

          <div className="channel-tag-container">{renderChannels}</div>
        </div>
        <div className={`search-btn ${allSelected ? 'complete' : ''}`} onClick={allSelected ? handleSearch : () => {}}>
          <div className="search-logo">
            <SearchIcon />
            <div className={`label`}>SEARCH</div>
          </div>
          {selectedChannel !== '' ? (
            <div className="selected-display">
              {`${selectedChannel} activities`}
              {dateFromDD ? ` from ${dateFromDD}/${dateFromMM}` : ''}
              {dateToDD ? ` to ${dateToDD}/${dateToMM}` : ''}
            </div>
          ) : (
            ''
          )}
        </div>
      </div>

      <div className={`${props.isOpened ? 'open' : ''} shadow`} onClick={props.closeMenu}>
        {''}
      </div>
    </div>
  );
};

function mapDispatchToProps(dispatch: any) {
  return {
    push: (url: string) => dispatch(push(url)),
  };
}

export default withRouter(connect(null, mapDispatchToProps)(SideBar));
